module Main (main) where

import Data.Traversable
import System.Random
import English
import Generate

main :: IO ()
main = do 
        for [1..100] $ \_ -> do
                                g <- newStdGen
                                putStrLn $ generateRandomWordWithStdGen syllables g
        putStrLn "100 syllables generated."
