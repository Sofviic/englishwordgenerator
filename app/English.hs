module English where

import Generate
import System.Random

{--

TODO: FGS split into multiple files instead of sectioning a single file! <- Non-idiomatic

--}

-- [1]: https://en.wikipedia.org/wiki/English_phonology#Syllable_structure
-- [2.o]: https://en.wikipedia.org/wiki/English_phonology#Onset
-- [2.c]: https://en.wikipedia.org/wiki/English_phonology#Coda
    
type Letter     = Char
type Vowel      = Char
type Consonant  = Char

vowelLetters :: [Vowel]
vowelLetters = "aeiou" -- not including j, y, w, h, or r

consonantLetters :: [Consonant]
consonantLetters = "bcdfghjklmnpqrstvwyxz" -- not including a, e, u, i, or o

--
monothongs :: [Vowel]
monothongs = "æɑɒɔɪeʌʊ"

dipthongs :: [[Vowel]]
dipthongs = ["aɪ","ɔɪ","aʊ"]

repVowel :: [Vowel] -> String
repVowel "æ" = "a"
repVowel "ɑ" = "a"
repVowel "ɒ" = "o"
repVowel "ɔ" = "ough"
repVowel "ɪ" = "i"
repVowel "e" = "e"
repVowel "ʌ" = "u"
repVowel "ʊ" = "oo"
repVowel "aɪ" = "i"
repVowel "ɔɪ" = "oi"
repVowel "aʊ" = "ou"

--
consonants :: [Consonant]
consonants = "mnŋptʧkbdʤgfθsʃhvðzʒlrjw"

repConsonant :: Consonant -> String
repConsonant 'ŋ' = "ng"
repConsonant 'ʧ' = "ch"
repConsonant 'ʤ' = "j"
repConsonant 'θ' = "th"
repConsonant 'ʃ' = "sh"
repConsonant 'ð' = "th"
repConsonant 'ʒ' = "si"
repConsonant 'j' = "y"
repConsonant c = [c]


stops :: [Consonant]
stops       = "bdgptk"      -- S

affricates :: [Consonant]
affricates  = "ʧʤ"          -- idk

approximant :: [Consonant]
approximant = "lrjw"        -- A

fricative,nvfricative,vfricative :: [Consonant]
fricative   = "fθsʃhvðzʒ"   -- F
nvfricative = "fθsʃh"       -- nF
vfricative  = "vðzʒ"        -- vF

nasals :: [Consonant]
nasals      = "mn"          -- N (not adding ŋ because it's exceptional)
------------------------------------------------------------------------

type Onset      = [Consonant]
type Nucleus    = [Vowel]
type Coda       = [Consonant]

-- [1]: 1, C, CC, or CCC are valid
onsetsFull :: [Onset]
onsetsFull = "" : 
    possibles 1 consonantLetters ++ 
    possibles 2 consonantLetters ++ 
    possibles 3 consonantLetters

-- [2.o]
onsets :: [Onset]
onsets = 
    [[c]            | c <- removes "ŋ" consonants] ++ -- C
    [[s,a]          | s <- stops, a <- removes "j" approximant] ++ -- SA
    [[nvf,a]        | nvf <- nvfricative ++ "v", a <- removes "j" approximant] ++ -- [nv]FA
    [[c,'j']        | c <- removes "ŋrjw" consonants] ++ -- Cj
    [['s',nvs]      | nvs <- "ptk"] ++ -- s[nv]S
    [['s',n]        | n <- nasals] ++ -- sN
    [['s',nvnsf]    | nvnsf <- "fθ"] ++ -- s[nv][ns]F
    [['s',nvs,a]    | nvs <- "ptk", a <- approximant] ++ -- s[nv]SA
    [['s',n,a]      | n <- nasals, a <- approximant] ++ -- sNA
    [['s',nvnsf,a]  | nvnsf <- "fθ", a <- approximant] ++ -- s[nv][ns]FA
    [""]

-- [1]: V is the only valid
nucleiFull :: [Nucleus]
nucleiFull = possibles 1 vowelLetters

nuclei :: [Nucleus]
nuclei = [[v] | v <- monothongs] ++ dipthongs -- V

-- [1]: 1, C, CC, CCC, or CCCC are valid
codasFull :: [Onset]
codasFull = "" : 
    possibles 1 consonantLetters ++ 
    possibles 2 consonantLetters ++ 
    possibles 3 consonantLetters ++ 
    possibles 4 consonantLetters

-- [2.c]
codas :: [Onset]
codas = 
    [[c]            | c <- consonants] ++ -- C
    [['l',s]        | s <- stops] ++ -- lS
    [['l',af]       | af <- affricates] ++ -- l(ʧ|ʤ)
    [['r',s]        | s <- stops] ++ -- rS
    [['r',af]       | af <- affricates] ++ -- r(ʧ|ʤ)
    [['l',f]        | f <- fricative] ++ -- lF
    [['r',f]        | f <- fricative] ++ -- rF
    [['l',n]        | n <- nasals] ++ -- lN
    [['r',n]        | n <- nasals] ++ -- rN
    [['r','l']] ++ -- rl
    ["mp", "nt", "nd", "nʧ", "nʤ", "ŋk"] ++ -- N[h](S|ʧ|ʤ)
    [[n,f]          | n <- nasals, f <- fricative] ++ -- NF
    ["ft", "sp", "st", "sk", "ʃt", "θt"] ++ -- [nv]F[nv]S
    ["zd", "ðd"] ++ -- [v]F[v]S
    ["fθ", "fθs"] ++ -- [nv]F[nv]F([nv]F)?
    ["pt", "kt"] ++ -- [nv]S[nv]S
    ["pts", "kts"] ++ -- [nv]S[nv]SF
    [[s,f]          | s <- stops, f <- fricative] ++ -- SF
    ["lmd", "lpt", "lps", "lfθ", "lvð", "lvf", "lts", "lst", "lkt", "lks"] ++ -- lCC(C)?
    ["rmd", "rmθ", "rpt", "rps", "rnd", "rts", "rst", "rld", "rkt"] ++ -- rCC(C)?
    --[[n,hs,s]       | n <- nasals, hs <- "ptdk", s <- stops] ++ -- N[h]SS
    --[[n,hs,f]       | n <- nasals, hs <- "ptdk", f <- fricative] ++ -- N[h]SF
    ["ksθ", "kst"] ++ -- OOO
    [""]
    

------------------------------------------------------------------------
possibles :: Int -> [a] -> [[a]]
possibles 0 _ = [[]]
possibles n x = [e:p | e <- x, p <- possibles (n-1) x]

slimList :: [a] -> [a]
slimList l = take (length l `div` 20) l

remove :: Eq a => a -> [a] -> [a]
remove x = filter (/= x)

removes :: Eq a => [a] -> [a] -> [a]
removes l s = [c | c <- s, not $ elem c l]

-- TODO: Add other english restrictions according to [1]

type Syllable = [Letter]

syllablesFull :: [Syllable]
syllablesFull = [o ++ n ++ c | o <- onsetsFull, n <- nucleiFull, c <- codasFull]

numOfPossibleSyllablesFull :: Int
numOfPossibleSyllablesFull = product . map length $ [onsetsFull,nucleiFull,codasFull]

genSyllableFull :: StdGen -> Syllable
genSyllableFull g = choose g onsetsFull ++ choose g nucleiFull ++ choose g codasFull


-- TODO: add optional t, d, or s at the end if allowed
syllables :: [Syllable]
syllables = [concat (map repConsonant o) ++ repVowel n ++ concat (map repConsonant c) | o <- onsets, n <- nuclei, c <- codas]
