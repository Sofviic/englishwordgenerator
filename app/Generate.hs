module Generate where

import System.Random

choose :: StdGen -> [a] -> a
choose g l = l !! i
                where (i, _) = randomR (0,(length l - 1)) g

generateRandomWord :: [a] -> Int -> a
generateRandomWord l n = choose (mkStdGen n) l

generateRandomWordWithStdGen :: [a] -> StdGen -> a
generateRandomWordWithStdGen = flip choose
